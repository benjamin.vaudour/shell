package console

import (
	"fmt"
	"framagit.org/benjamin.vaudour/converter/v2"
	"framagit.org/benjamin.vaudour/shell/v2/scanner"
	"strings"
)

// PromptFunc est une fonction qui affiche une invite de commande et retourne la saisie brute.
type PromptFunc func(string) (string, error)

// ParsedPromptFunc est une fonction qui affiche une invite de commande et retourne la saisie parsée.
type ParsedPromptFunc[T any] func(string) (T, error)

// ConvFunc est une fonction qui convertit une chaîne de caractère en un type donné.
type ConvFunc[T any] func(string) (T, error)

// PromptOf transforme un prompter en fonction promptable.
func PromptOf(p Prompter) PromptFunc {
	return p.Prompt
}

func singleConv[T any](s string) (out T, err error) {
	if !converter.Convert(s, &out) {
		err = fmt.Errorf("Failed to convert %s", s)
	}
	return
}

func boolConv(s string) (out bool, err error) {
	s = strings.ToLower(s)
	switch s {
	case "oui", "o", "yes", "y", "1", "true", "t", "on":
		out = true
	case "non", "n", "no", "0", "false", "f", "off":
		out = false
	default:
		err = fmt.Errorf("%s: not a boolean", s)
	}
	return
}

func strConv(s string) (out string, err error) {
	out = s
	return
}

// ParsedPrompt transforme un prompt en prompt de donnée parsée.
func ParsedPrompt[T any](sp PromptFunc, conv ConvFunc[T]) ParsedPromptFunc[T] {
	return func(p string) (out T, err error) {
		var sout string
		if sout, err = sp(p); err == nil {
			out, err = conv(sout)
		}
		return
	}
}

// PromptInt transforme un prompt en prompt d’entier.
func PromptInt(sp PromptFunc) ParsedPromptFunc[int] {
	return ParsedPrompt(sp, singleConv[int])
}

// PromptUint transforme un prompt en prompt d’entier non signé.
func PromptUint(sp PromptFunc) ParsedPromptFunc[uint] {
	return ParsedPrompt(sp, singleConv[uint])
}

// PromptFloat transforme un prompt en prompt de nombre décimal.
func PromptFloat(sp PromptFunc) ParsedPromptFunc[float64] {
	return ParsedPrompt(sp, singleConv[float64])
}

// PromptBool transforme un prompt en prompt de booléen.
//
// Valeurs autorisée :
// - true : O(ui), Y(es), t(rue), 1, on
// - false : N(on), f(alse), 0, off
// La valeur est insensible à la casse.
func PromptBool(sp PromptFunc) ParsedPromptFunc[bool] {
	return ParsedPrompt(sp, boolConv)
}

// MultiplePrompt transforme un prompt en prompt de valeurs multiples.
// Si aucun tokenizer n’est fourni, il considère la chaîne globale
// comme des mots séparés par des blancs.
func MultiplePrompt[T any](
	sp PromptFunc,
	conv ConvFunc[T],
	t ...scanner.Tokenizer,
) ParsedPromptFunc[[]T] {
	return func(p string) (out []T, err error) {
		var sout string
		if sout, err = sp(p); err == nil {
			var tk scanner.Tokenizer
			if len(t) > 0 {
				tk = t[0]
			} else {
				tk = scanner.NewTokenizer(false, false)
			}
			sc := scanner.NewScanner(strings.NewReader(sout), tk)
			var tmp []T
			var v T
			for sc.Scan() {
				if v, err = conv(sc.Text()); err != nil {
					return
				}
				tmp = append(tmp, v)
			}
			out = tmp
		}
		return
	}
}

// PromptSlice transform un prompt en prompt de slice de strings.
func PromptSlice(sp PromptFunc, t ...scanner.Tokenizer) ParsedPromptFunc[[]string] {
	return MultiplePrompt(sp, strConv, t...)
}

// PromptSliceInt transform un prompt en prompt de slice d’entiers.
func PromptSliceInt(sp PromptFunc, t ...scanner.Tokenizer) ParsedPromptFunc[[]int] {
	return MultiplePrompt(sp, singleConv[int], t...)
}

// PromptSliceUint transform un prompt en prompt de slice d’entiers non signés.
func PromptSliceUint(sp PromptFunc, t ...scanner.Tokenizer) ParsedPromptFunc[[]uint] {
	return MultiplePrompt(sp, singleConv[uint], t...)
}

// PromptSliceFloat transform un prompt en prompt de slice de décimaux.
func PromptSliceFloat(sp PromptFunc, t ...scanner.Tokenizer) ParsedPromptFunc[[]float64] {
	return MultiplePrompt(sp, singleConv[float64], t...)
}

// PromptSliceBool transform un prompt en prompt de slice de booléens.
func PromptSliceBool(sp PromptFunc, t ...scanner.Tokenizer) ParsedPromptFunc[[]bool] {
	return MultiplePrompt(sp, boolConv, t...)
}
