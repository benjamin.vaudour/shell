package errors

import (
	"fmt"
)

type Error struct {
	msg  string
	code int
}

func (err *Error) Error() string {
	return err.msg
}

func (err *Error) Code() int {
	return err.code
}

func New(code int, msg string) *Error {
	return &Error{
		msg:  msg,
		code: code,
	}
}

func Newf(code int, tmpl string, args ...any) *Error {
	return New(code, fmt.Sprintf(tmpl, args...))
}
