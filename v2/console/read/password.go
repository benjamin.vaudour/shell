package read

import (
	"bufio"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"
)

// termEcho turns terminal echo on or off.
func termEcho(on bool) error {
	// Common settings and variables for both stty calls.
	attrs := syscall.ProcAttr{
		Dir:   "",
		Env:   []string{},
		Files: []uintptr{os.Stdin.Fd(), os.Stdout.Fd(), os.Stderr.Fd()},
		Sys:   nil,
	}
	var ws syscall.WaitStatus
	cmd := "echo"
	if on == false {
		cmd = "-echo"
	}

	// Enable/disable echoing.
	pid, err := syscall.ForkExec(
		"/bin/stty",
		[]string{"stty", cmd},
		&attrs,
	)
	if err != nil {
		return err
	}

	// Wait for the stty process to complete.
	_, err = syscall.Wait4(pid, &ws, 0, nil)
	return err
}

// Password agit comme String mais n’affiche pas ce qui est saisi.
func Password(prompt string) (result string, err error) {
	fmt.Print(prompt)

	// Catch a ^C interrupt.
	// Make sure that we reset term echo before exiting.
	signalChannel := make(chan os.Signal, 1)
	signal.Notify(signalChannel, os.Interrupt)
	go func() {
		for _ = range signalChannel {
			fmt.Println("\n^C interrupt.")
			termEcho(true)
			os.Exit(1)
		}
	}()

	// Echo is disabled, now grab the data.
	if err = termEcho(false); err != nil {
		return
	}
	sc := bufio.NewScanner(os.Stdin)
	if sc.Scan() {
		result = sc.Text()
	} else {
		err = sc.Err()
	}
	termEcho(true) // always re-enable terminal echo
	fmt.Println("")
	result = strings.TrimSpace(result)
	return
}
