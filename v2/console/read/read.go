package read

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"framagit.org/benjamin.vaudour/collection/v2"
	"framagit.org/benjamin.vaudour/shell/v2/console"
	"framagit.org/benjamin.vaudour/shell/v2/scanner"
)

// String retourne la chaîne de caractères saisie.
func String(prompt string) (result string, err error) {
	fmt.Print(prompt)
	sc := bufio.NewScanner(os.Stdin)
	if sc.Scan() {
		result = sc.Text()
	} else {
		err = sc.Err()
	}
	return
}

// Int retourne l’entier saisi.
func Int(prompt string) (result int, err error) {
	return console.PromptInt(String)(prompt)
}

// Uint retourne l’entier non signé saisi.
func Uint(prompt string) (result uint, err error) {
	return console.PromptUint(String)(prompt)
}

// Float retourne le nombre décimal saisi.
func Float(prompt string) (result float64, err error) {
	return console.PromptFloat(String)(prompt)
}

// Bool retourne le booléen saisi.
func Bool(prompt string) (result bool, err error) {
	return console.PromptBool(String)(prompt)
}

// Slice retourne les mots saisis.
func Slice(prompt string, t ...scanner.Tokenizer) (result []string, err error) {
	return console.PromptSlice(String, t...)(prompt)
}

// SliceInt retourne les entiers saisis.
func SliceInt(prompt string, t ...scanner.Tokenizer) (result []int, err error) {
	return console.PromptSliceInt(String, t...)(prompt)
}

// SliceUint retourne les entiers non signés saisis.
func SliceUint(prompt string, t ...scanner.Tokenizer) (result []uint, err error) {
	return console.PromptSliceUint(String, t...)(prompt)
}

// SliceFloat retourne les nombres décimaux saisis.
func SliceFloat(prompt string, t ...scanner.Tokenizer) (result []float64, err error) {
	return console.PromptSliceFloat(String, t...)(prompt)
}

// SliceBool retourne les booléens saisis.
func SliceBool(prompt string, t ...scanner.Tokenizer) (result []bool, err error) {
	return console.PromptSliceBool(String, t...)(prompt)
}

//Default lance une invite de commande attendant une réponse optionnelle
func Default[T any](prompt string, def T, pp console.ParsedPromptFunc[T]) (result T, err error) {
	var err2 error
	if result, err2 = pp(prompt); err2 != nil {
		result = def
	}
	return
}

func question[T any](q string, def T, pp console.ParsedPromptFunc[T]) (result T) {
	prompt := fmt.Sprintf("%s (default: %v) ", q, def)
	result, _ = Default(prompt, def, pp)
	return
}

// Question invite à saisir une chaîne.
// Si aucune chaîne n’est saisie, def est retourné.
func Question(q string, def string) (result string) {
	return question(q, def, String)
}

// QuestionInt invite à saisir un entier.
// Si aucun entier n’est saisi, def est retourné.
func QuestionInt(q string, def int) (result int) {
	return question(q, def, Int)
}

// QuestionUint invite à saisir un entier non signé.
// Si aucun entier non signé n’est saisi, def est retourné.
func QuestionUint(q string, def uint) (result uint) {
	return question(q, def, Uint)
}

// QuestionFloat invite à saisir un nombre décimal.
// Si aucun nombre décimal n’est saisi, def est retourné.
func QuestionFloat(q string, def float64) (result float64) {
	return question(q, def, Float)
}

// QuestionBool invite à saisir un booléen.
// Si aucun booléen n’est saisi, def est retourné.
func QuestionBool(q string, def bool) (result bool) {
	choices := "o/N"
	if def {
		choices = "O/n"
	}
	prompt := fmt.Sprintf("%s [%s] ", q, choices)
	if r, err := String(prompt); err == nil && len(r) > 0 {
		switch r[0] {
		case 'Y', 'y', 'O', 'o', '1':
			return true
		case 'N', 'n', '0':
			return false
		}
	}
	return def
}

// QuestionChoice invite à saisir un chaîne parmi un choix donné.
// Si aucun choix n’est effectué, retourne def.
func QuestionChoice(q string, def string, choices []string) (result string) {
	set := collection.SliceToSet(choices)
	prompt := fmt.Sprintf("%s [%s] (default: %v) ", q, strings.Join(choices, "|"), def)
	if r, err := String(prompt); err == nil && set.Contains(r) {
		return r
	}
	return def
}
