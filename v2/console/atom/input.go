package atom

import (
	"bufio"
	"io"
	"strconv"
	"time"
)

func timeout() <-chan time.Time {
	return time.After(50 * time.Millisecond)
}

type nexter struct {
	r   Char
	err error
}

type input struct {
	buf     *bufio.Reader
	next    <-chan nexter
	pending []Char
}

func (in *input) shift() (r Char) {
	if len(in.pending) > 0 {
		r, in.pending = in.pending[0], in.pending[1:]
	}
	return
}

func (in *input) push(r Char) Char {
	in.pending = append(in.pending, r)
	return r
}

func (in *input) clear() {
	in.pending = in.pending[:0]
}

func newInput(r io.Reader) *input {
	return &input{
		buf: bufio.NewReader(r),
	}
}

func (in *input) restart() {
	next := make(chan nexter, 200)
	go func() {
		for {
			var n nexter
			n.r, _, n.err = in.buf.ReadRune()
			next <- n
			if n.err != nil || n.r == Lf || n.r == Cr || n.r == C_C || n.r == C_D {
				close(next)
				return
			}
		}
	}()
	in.next = next
}

func (in *input) isWaiting() bool {
	return len(in.next) > 0
}

func (in *input) nextc() (r Char, err error) {
	select {
	case n, ok := <-in.next:
		if !ok {
			err = ErrInternal
			return
		} else if n.err != nil {
			r, err = 0, n.err
		} else {
			r = in.push(n.r)
		}
	}
	return
}

func (in *input) nextt() (r Char, err error) {
	select {
	case n, ok := <-in.next:
		if !ok {
			err = ErrInternal
		} else if n.err != nil {
			r, err = 0, n.err
		} else {
			r = in.push(n.r)
		}
	case <-timeout():
		r, err = in.shift(), ErrTimeout
	}
	return
}

func (in *input) escO(key *Key) (err error) {
	if key.r, err = in.nextt(); err != nil {
		if err == ErrTimeout {
			key.s, err = AS_O, nil
		}
		return
	}
	if key.r >= '0' && key.r <= '9' {
		key.s = Sequence(key.r << 8)
		if key.r, err = in.nextt(); err != nil {
			if err == ErrTimeout {
				err = nil
			}
			*key = KeyNil
			return
		}
	}
	key.s |= Sequence(key.r)
	return
}

func (in *input) escBracket(key *Key) (err error) {
	if key.r, err = in.nextt(); err != nil {
		if err == ErrTimeout {
			err = nil
		}
		*key = KeyNil
		return
	}
	switch key.r {
	case 'A':
		key.s = Up
	case 'B':
		key.s = Down
	case 'C':
		key.s = Right
	case 'D':
		key.s = Left
	case 'F':
		key.s = End
	case 'H':
		key.s = Home
	case 'Z':
		key.s = S_Tab
	case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
		err = in.escNumberBracket(key)
	default:
		*key = KeyNil
	}
	return
}

func (in *input) escNumberBracket(key *Key) (err error) {
	num := []Char{key.r}
loop:
	for {
		if key.r, err = in.nextt(); err != nil {
			if err == ErrTimeout {
				err = nil
			}
			*key = KeyNil
			return
		}
		switch key.r {
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			num = append(num, key.r)
		case '~':
			err = in.escTildeBracket(key, num)
			break loop
		case ';':
			err = in.escSemiColonBracket(key, num)
			break loop
		default:
			*key = KeyNil
			break loop
		}
	}
	return
}

func (in *input) escTildeBracket(key *Key, num []Char) (err error) {
	x, _ := strconv.ParseInt(string(num), 10, 32)
	key.s = Sequence(x)
	return
}

func (in *input) escSemiColonBracket(key *Key, num []Char) (err error) {
	x, _ := strconv.ParseInt(string(num), 10, 32)
	ok := x != 1
	if ok {
		key.s = Sequence(x)
	}
	if key.r, err = in.nextt(); err != nil {
		if err == ErrTimeout {
			err = nil
		}
		*key = KeyNil
		return
	}
	key.s |= Sequence(key.r << 8)
	if key.r, err = in.nextt(); err != nil {
		if err == ErrTimeout {
			err = nil
		}
		*key = KeyNil
		return
	}
	if key.r == '~' {
		if !ok {
			*key = KeyNil
		}
	} else if !ok {
		key.s |= Sequence(key.r)
	} else {
		*key = KeyNil
	}
	return
}

func (in *input) nextChar() (key Key, err error) {
	if len(in.pending) > 0 {
		key.r = in.shift()
		return
	}

	if key.r, err = in.nextc(); err != nil || key.r != Esc {
		in.shift()
		return
	}

	if key.r, err = in.nextt(); err != nil {
		if err == ErrTimeout {
			err = nil
		} else {
			key = KeyNil
		}
		return
	}

	switch key.r {
	case Bs:
		key.s = A_Bs
	case 'O':
		err = in.escO(&key)
	case '[':
		err = in.escBracket(&key)
	case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z':
		key.s = Sequence(key.r << 16)
	default:
		key = KeyNil
	}
	if key.IsSequence() {
		key.r = 0
	}
	in.clear()
	return
}
