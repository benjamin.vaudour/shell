package atom

import (
	"errors"
)

// Char représente un caractère UTF-8 saisi
// via une touche ou une combinaison de touches.
type Char = rune

// Sequence représente une séquence de caractères UTF-8
// saisis via une touche ou une combinaison de touche.
type Sequence rune

// Key représente un caractère ou un séquence de caractères.
type Key struct {
	r Char
	s Sequence
}

// Char retourne un caractère.
func (k Key) Char() Char {
	return k.r
}

// Sequence retourne une séquence.
func (k Key) Sequence() Sequence {
	return k.s
}

// IsSequence retourne vrai si k représente une séquence.
func (k Key) IsSequence() bool {
	return k.s != 0
}

// IsChar retourne vrai si k représente un caractère.
func (k Key) IsChar() bool {
	return k.r != 0
}

// IsNil retourne vrai si k n’est ni une séquence ni un caractère.
func (k Key) IsNil() bool {
	return !k.IsChar() && !k.IsSequence()
}

var (
	KeyNil = Key{}
)

const (
	Bs = '\u007f' // \?
	Sp = '\u0020'
)

const (
	C_A rune = iota + 1
	C_B
	C_C
	C_D
	C_E
	C_F
	C_G
	C_H
	C_I // \t
	C_J // \n
	C_K
	C_L
	C_M // \r
	C_N
	C_O
	C_P
	C_Q
	C_R
	C_S
	C_T
	C_U
	C_V
	C_W
	C_X
	C_Y
	C_Z
	Esc // \e
)

const (
	Tab = C_I
	Lf  = C_J
	Cr  = C_M
)

const (
	shift Sequence = (iota + '2') << 8
	alt
	altShit
	ctrl
	ctrlShift
	ctrlAlt
	ctrlAltShift
)

const (
	MaskKey Sequence = 127 // Permet, en appliquant &, de récupérer la séquence sans les touches modificatrices
)

const (
	Up    Sequence = iota + 'A' // ^[[A
	Down                        // ^[[B
	Right                       // ^[[C
	Left                        // ^[[D
)

const (
	Ins Sequence = iota + 2 // ^[[2~
	Del                     // ^[[3~
	_
	PgUp   Sequence = 5   // ^[[5~
	PgDown          = 6   // ^[[6~
	End    Sequence = 'F' // ^[[F
	Home   Sequence = 'H' // ^[[H
)

const (
	F1 Sequence = iota + 'P' // ^[OP
	F2                       // ^[OQ
	F3                       // ^[OR
	F4                       // ^[OS
)

const (
	F5 Sequence = iota + 15 // ^[[15~
	_
	F6  // ^[[17~
	F7  // ^[[18~
	F8  // ^[[19~
	F9  // ^[[20~
	F10 // ^[[21~
	_
	F11 // ^[[23~
	F12 // ^[[24~
)

const (
	S_Up     = shift | Up            // ^[[1;2A
	S_down   = shift | Down          // ^[[1;2B
	S_Right  = shift | Right         // ^[[1;2C
	S_Left   = shift | Left          // ^[[1;2D
	S_Ins    = shift | Ins           // ^[[2;2~
	S_Del    = shift | Del           // ^[[3;2~
	S_PgUp   = shift | PgUp          // ^[[5;2~
	S_PgDown = shift | PgDown        // ^[[6;2~
	S_End    = shift | End           // ^[[1;2F
	S_Home   = shift | Home          // ^[[1;2H
	S_F1     = shift | F1            // ^[O2P
	S_F2     = shift | F2            // ^[O2Q
	S_F3     = shift | F3            // ^[O2R
	S_F4     = shift | F4            // ^[O2S
	S_F5     = shift | F5            // ^[[15;2~
	S_F6     = shift | F6            // ^[[17;2~
	S_F7     = shift | F7            // ^[[18;2~
	S_F8     = shift | F8            // ^[[19;2~
	S_F9     = shift | F9            // ^[[20;2~
	S_F10    = shift | F10           // ^[[21;2~
	S_F11    = shift | F11           // ^[[23;2~
	S_F12    = shift | F12           // ^[[24;2~
	S_Tab    = shift | Sequence(Tab) // ^[[Z

	A_Up     = alt | Up           // ^[[1;3A
	A_down   = alt | Down         // ^[[1;3B
	A_Right  = alt | Right        // ^[[1;3C
	A_Left   = alt | Left         // ^[[1;3D
	A_Ins    = alt | Ins          // ^[[2;3~
	A_Del    = alt | Del          // ^[[3;3~
	A_PgUp   = alt | PgUp         // ^[[5;3~
	A_PgDown = alt | PgDown       // ^[[6;3~
	A_End    = alt | End          // ^[[1;3F
	A_Home   = alt | Home         // ^[[1;3H
	A_F1     = alt | F1           // ^[O3P
	A_F2     = alt | F2           // ^[O3Q
	A_F3     = alt | F3           // ^[O3R
	A_F4     = alt | F4           // ^[O3S
	A_F5     = alt | F5           // ^[[15;3~
	A_F6     = alt | F6           // ^[[17;3~
	A_F7     = alt | F7           // ^[[18;3~
	A_F8     = alt | F8           // ^[[19;3~
	A_F9     = alt | F9           // ^[[20;3~
	A_F10    = alt | F10          // ^[[21;3~
	A_F11    = alt | F11          // ^[[23;3~
	A_F12    = alt | F12          // ^[[24;3~
	A_Bs     = alt | Sequence(Bs) // ^[<BACK>

	AS_Up     = altShit | Up     // ^[[1;4A
	AS_down   = altShit | Down   // ^[[1;4B
	AS_Right  = altShit | Right  // ^[[1;4C
	AS_Left   = altShit | Left   // ^[[1;4D
	AS_Ins    = altShit | Ins    // ^[[2;4~
	AS_Del    = altShit | Del    // ^[[3;4~
	AS_PgUp   = altShit | PgUp   // ^[[5;4~
	AS_PgDown = altShit | PgDown // ^[[6;4~
	AS_End    = altShit | End    // ^[[1;4F
	AS_Home   = altShit | Home   // ^[[1;4H
	AS_F1     = altShit | F1     // ^[O4P
	AS_F2     = altShit | F2     // ^[O4Q
	AS_F3     = altShit | F3     // ^[O4R
	AS_F4     = altShit | F4     // ^[O4S
	AS_F5     = altShit | F5     // ^[[15;4~
	AS_F6     = altShit | F6     // ^[[17;4~
	AS_F7     = altShit | F7     // ^[[18;4~
	AS_F8     = altShit | F8     // ^[[19;4~
	AS_F9     = altShit | F9     // ^[[20;4~
	AS_F10    = altShit | F10    // ^[[21;4~
	AS_F11    = altShit | F11    // ^[[23;4~
	AS_F12    = altShit | F12    // ^[[24;4~

	C_Up     = ctrl | Up     // ^[[1;5A
	C_down   = ctrl | Down   // ^[[1;5B
	C_Right  = ctrl | Right  // ^[[1;5C
	C_Left   = ctrl | Left   // ^[[1;5D
	C_Ins    = ctrl | Ins    // ^[[2;5~
	C_Del    = ctrl | Del    // ^[[3;5~
	C_PgUp   = ctrl | PgUp   // ^[[5;5~
	C_PgDown = ctrl | PgDown // ^[[6;5~
	C_End    = ctrl | End    // ^[[1;5F
	C_Home   = ctrl | Home   // ^[[1;5H
	C_F1     = ctrl | F1     // ^[O5P
	C_F2     = ctrl | F2     // ^[O5Q
	C_F3     = ctrl | F3     // ^[O5R
	C_F4     = ctrl | F4     // ^[O5S
	C_F5     = ctrl | F5     // ^[[15;5~
	C_F6     = ctrl | F6     // ^[[17;5~
	C_F7     = ctrl | F7     // ^[[18;5~
	C_F8     = ctrl | F8     // ^[[19;5~
	C_F9     = ctrl | F9     // ^[[20;5~
	C_F10    = ctrl | F10    // ^[[21;5~
	C_F11    = ctrl | F11    // ^[[23;5~
	C_F12    = ctrl | F12    // ^[[34;5~

	CS_Up     = ctrlShift | Up     // ^[[1;6A
	CS_down   = ctrlShift | Down   // ^[[1;6B
	CS_Right  = ctrlShift | Right  // ^[[1;6C
	CS_Left   = ctrlShift | Left   // ^[[1;6D
	CS_Ins    = ctrlShift | Ins    // ^[[2;6~
	CS_Del    = ctrlShift | Del    // ^[[3;6~
	CS_PgUp   = ctrlShift | PgUp   // ^[[5;6~
	CS_PgDown = ctrlShift | PgDown // ^[[6;6~
	CS_End    = ctrlShift | End    // ^[[1;6F
	CS_Home   = ctrlShift | Home   // ^[[1;6H
	CS_F1     = ctrlShift | F1     // ^[O6P
	CS_F2     = ctrlShift | F2     // ^[O6Q
	CS_F3     = ctrlShift | F3     // ^[O6R
	CS_F4     = ctrlShift | F4     // ^[O6S
	CS_F5     = ctrlShift | F5     // ^[[15;6~
	CS_F6     = ctrlShift | F6     // ^[[17;6~
	CS_F7     = ctrlShift | F7     // ^[[18;6~
	CS_F8     = ctrlShift | F8     // ^[[19;6~
	CS_F9     = ctrlShift | F9     // ^[[20;6~
	CS_F10    = ctrlShift | F10    // ^[[21;6~
	CS_F11    = ctrlShift | F11    // ^[[23;6~
	CS_F12    = ctrlShift | F12    // ^[[24;6~

	CA_Up     = ctrlAlt | Up     // ^[[1;7A
	CA_down   = ctrlAlt | Down   // ^[[1;7B
	CA_Right  = ctrlAlt | Right  // ^[[1;7C
	CA_Left   = ctrlAlt | Left   // ^[[1;7D
	CA_Ins    = ctrlAlt | Ins    // ^[[2;7~
	CA_Del    = ctrlAlt | Del    // ^[[3;7~
	CA_PgUp   = ctrlAlt | PgUp   // ^[[5;7~
	CA_PgDown = ctrlAlt | PgDown // ^[[6;7~
	CA_End    = ctrlAlt | End    // ^[[1;7F
	CA_Home   = ctrlAlt | Home   // ^[[1;7H
	CA_F1     = ctrlAlt | F1     // ^[O7P
	CA_F2     = ctrlAlt | F2     // ^[O7Q
	CA_F3     = ctrlAlt | F3     // ^[O7R
	CA_F4     = ctrlAlt | F4     // ^[O7S
	CA_F5     = ctrlAlt | F5     // ^[[15;7~
	CA_F6     = ctrlAlt | F6     // ^[[17;7~
	CA_F7     = ctrlAlt | F7     // ^[[18;7~
	CA_F8     = ctrlAlt | F8     // ^[[19;7~
	CA_F9     = ctrlAlt | F9     // ^[[20;7~
	CA_F10    = ctrlAlt | F10    // ^[[21;7~
	CA_F11    = ctrlAlt | F11    // ^[[23;7~
	CA_F12    = ctrlAlt | F12    // ^[[24;7~

	CAS_Up     = ctrlAltShift | Up     // ^[[1;8A
	CAS_down   = ctrlAltShift | Down   // ^[[1;8B
	CAS_Right  = ctrlAltShift | Right  // ^[[1;8C
	CAS_Left   = ctrlAltShift | Left   // ^[[1;8D
	CAS_Ins    = ctrlAltShift | Ins    // ^[[2;8~
	CAS_Del    = ctrlAltShift | Del    // ^[[3;8~
	CAS_PgUp   = ctrlAltShift | PgUp   // ^[[5;8~
	CAS_PgDown = ctrlAltShift | PgDown // ^[[6;8~
	CAS_End    = ctrlAltShift | End    // ^[[1;8F
	CAS_Home   = ctrlAltShift | Home   // ^[[1;8H
	CAS_F1     = ctrlAltShift | F1     // ^[O8P
	CAS_F2     = ctrlAltShift | F2     // ^[O8Q
	CAS_F3     = ctrlAltShift | F3     // ^[O8R
	CAS_F4     = ctrlAltShift | F4     // ^[O8S
	CAS_F5     = ctrlAltShift | F5     // ^[[15;8~
	CAS_F6     = ctrlAltShift | F6     // ^[[17;8~
	CAS_F7     = ctrlAltShift | F7     // ^[[18;8~
	CAS_F8     = ctrlAltShift | F8     // ^[[19;8~
	CAS_F9     = ctrlAltShift | F9     // ^[[20;8~
	CAS_F10    = ctrlAltShift | F10    // ^[[21;8~
	CAS_F11    = ctrlAltShift | F11    // ^[[23;8~
	CAS_F12    = ctrlAltShift | F12    // ^[[24;8~
)

const (
	A_A Sequence = ('a' + iota) << 16 // ^[a
	A_B                               // ^[b
	A_C                               // ^[c
	A_D                               // ^[d
	A_E                               // ^[e
	A_F                               // ^[f
	A_G                               // ^[g
	A_H                               // ^[h
	A_I                               // ^[i
	A_J                               // ^[j
	A_K                               // ^[k
	A_L                               // ^[l
	A_M                               // ^[m
	A_N                               // ^[n
	A_O                               // ^[o
	A_P                               // ^[p
	A_Q                               // ^[q
	A_R                               // ^[r
	A_S                               // ^[s
	A_T                               // ^[t
	A_U                               // ^[u
	A_V                               // ^[v
	A_W                               // ^[w
	A_X                               // ^[x
	A_Y                               // ^[y
	A_Z                               // ^[z
)

const (
	AS_A Sequence = ('A' + iota) << 16 // ^[A
	AS_B                               // ^[B
	AS_C                               // ^[C
	AS_D                               // ^[D
	AS_E                               // ^[E
	AS_F                               // ^[F
	AS_G                               // ^[G
	AS_H                               // ^[H
	AS_I                               // ^[I
	AS_J                               // ^[J
	AS_K                               // ^[K
	AS_L                               // ^[L
	AS_M                               // ^[M
	AS_N                               // ^[N
	AS_O                               // ^[O
	AS_P                               // ^[P
	AS_Q                               // ^[Q
	AS_R                               // ^[R
	AS_S                               // ^[S
	AS_T                               // ^[T
	AS_U                               // ^[U
	AS_V                               // ^[V
	AS_W                               // ^[W
	AS_X                               // ^[X
	AS_Y                               // ^[Y
	AS_Z                               // ^[Z
)

var (
	ErrInternal = errors.New("internal error")
	ErrTimeout  = errors.New("timeout")

	evChar = map[int64]Sequence{
		2:  Ins,
		3:  Del,
		4:  End,
		5:  PgUp,
		6:  PgDown,
		7:  Home,
		8:  End,
		15: F5,
		17: F6,
		18: F7,
		19: F8,
		20: F9,
		21: F10,
		23: F11,
		24: F12,
	}
)
