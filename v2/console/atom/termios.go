package atom

import (
	"os"
	"strings"
	"syscall"
	"unsafe"
)

const (
	cs8        = syscall.CS8
	echo       = syscall.ECHO
	tcgets     = syscall.TCGETS
	tcsets     = syscall.TCSETS
	icanon     = syscall.ICANON
	icrnl      = syscall.ICRNL
	iexten     = syscall.IEXTEN
	inpck      = syscall.INPCK
	ioctl      = syscall.SYS_IOCTL
	isig       = syscall.ISIG
	istrip     = syscall.ISTRIP
	ixon       = syscall.IXON
	opost      = syscall.OPOST
	tiocgwinsz = syscall.TIOCGWINSZ
	vmin       = syscall.VMIN
	vtime      = syscall.VTIME
)

var (
	stdin  = syscall.Stdin
	stdout = syscall.Stdout
)

type termios struct {
	syscall.Termios
}

func (mode *termios) applyMode() (err error) {
	_, _, errno := syscall.Syscall(ioctl, uintptr(stdin), tcsets, uintptr(unsafe.Pointer(mode)))
	if errno != 0 {
		return errno
	}
	return nil
}

func newTermios(handle int) (mode *termios, err error) {
	mode = new(termios)
	_, _, errno := syscall.Syscall(ioctl, uintptr(handle), tcgets, uintptr(unsafe.Pointer(mode)))
	if errno != 0 {
		err = errno
	}
	return
}

func terminalSupported() bool {
	bad := map[string]bool{"": true, "dumb": true, "cons25": true}
	return !bad[strings.ToLower(os.Getenv("TERM"))]
}

type TerminalSize struct {
	row, col       uint16
	xpixel, ypixel uint16
}

func (ts TerminalSize) Width() int  { return int(ts.col) }
func (ts TerminalSize) Height() int { return int(ts.row) }

// GetTerminalSize retourne les dimensions actuelles du terminal.
func GetTerminalSize() (ts TerminalSize, ok bool) {
	sc, _, _ := syscall.Syscall(ioctl, uintptr(stdout), tiocgwinsz, uintptr(unsafe.Pointer(&ts)))
	ok = int(sc) >= 0
	return
}

func checkOutput() (useCHA bool) {
	// xterm is known to support CHA
	useCHA = strings.Contains(strings.ToLower(os.Getenv("TERM")), "xterm")
	return
}
