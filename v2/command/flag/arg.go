package flag

import (
	"fmt"
)

// Arg représente un argument de commande
type Arg struct {
	name  string
	usage string
	value *[]string
	nb    int
}

// Parse parse l’argument à partir de l’entrée fournie
// et retourne les éléments restant à parser.
// Si le parsage échoue, une erreur est retournée.
func (a *Arg) Parse(args []string) (next []string, err error) {
	l := len(args)
	switch a.nb {
	case -1:
		*a.value = make([]string, l)
		copy(*a.value, args)
	case 0:
		if l > 0 {
			next, *a.value = args[1:], []string{args[0]}
		}
	default:
		if l < a.nb {
			err = fmt.Errorf("Not enough args for %s", a.name)
		} else {
			*a.value = make([]string, a.nb)
			copy(*a.value, args[:a.nb])
			next = args[a.nb:]
		}
	}
	return
}

// IsParsed retourne vrai si l’argument a déjà été parsé
func (a *Arg) IsParsed() bool {
	return len(*a.value) > 0
}

// Reset réinitialise le parsage
func (a *Arg) Reset() {
	*a.value = []string{}
}

// Name retourne le nom de l’argument
func (a *Arg) Name() string { return a.name }

// Usage retourne l’aide sur l’utilisation de l’argument
func (a *Arg) Usage() string { return a.usage }

// NewArg retourne un nouvel argument avec son nom, sa description
// le nombre d’éléments attendu et un pointeur vers la valeur parsée.
//
// Si nbargs < 0, l’argument peut recevoir un nombre d’éléments indéterminé.
// Si nbargs = 0, l’argument est facultatif.
func NewArg(name, usage string, nbargs int, value *[]string) *Arg {
	if nbargs < 0 {
		nbargs = -1
	}
	return &Arg{
		name:  name,
		usage: usage,
		value: value,
		nb:    nbargs,
	}
}
