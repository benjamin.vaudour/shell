package flag

import (
	"framagit.org/benjamin.vaudour/collection/v2"
)

func letters(r rune) []rune {
	out := make([]rune, 26)
	for i := range out {
		out[i] = r + rune(i)
	}
	return out
}

var shortFlags = collection.SliceToSet(append(letters('a'), letters('A')...))

func isShort(e string) bool {
	re := []rune(e)
	return len(re) == 2 && e[0] == '-' && shortFlags.Contains(re[1])
}

func isLong(e string) bool {
	return len(e) > 2 && e[:2] == "--"
}

func isCombined(e string) bool {
	if len(e) <= 2 || e[0] != '-' {
		return false
	}
	for _, r := range e[1:] {
		if !shortFlags.Contains(r) {
			return false
		}
	}
	return true
}

func formatArg(e string) []string {
	if isCombined(e) {
		var out []string
		for _, r := range e[1:] {
			out = append(out, string([]rune{'-', r}))
		}
		return out
	}
	return []string{e}
}

func formatArgs(args []string) []string {
	var out []string
	ignore := false
	for _, e := range args {
		if ignore {
			out = append(out, e)
		} else {
			ignore = e == "--"
			out = append(out, formatArg(e)...)
		}
	}
	return out
}
