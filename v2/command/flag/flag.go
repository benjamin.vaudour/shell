package flag

import (
	"fmt"
	"framagit.org/benjamin.vaudour/converter/v2"
	"framagit.org/benjamin.vaudour/shell/v2/command"
	"strings"
)

// ConversionFunc est une fonction de conversion.
//
// Les éléments retournés sont :
// - next : liste des éléments restants à parser
// - val : la valeur parsée
// - err : une erreur si le parsage échoue
type ConversionFunc[T any] func([]string, *flag[T]) ([]string, T, int, error)

type callbackFunc[T any] func([]string, string) ([]string, T, int, error)

// Flag représente un argument nommé de commande
type Flag interface {
	command.Flag
	capacity() int
	nbParsed() int
	def() any
}

type flag[T any] struct {
	name       string
	flags      []string
	fval       string
	usage      string
	multiple   int
	value      *T
	defaultval T
	parsed     int
	conv       ConversionFunc[T]
}

// Parse parse l’argument à partir de l’entrée fournie
// et retourne les éléments restant à parser.
// Si le parsage échoue, une erreur est retournée.
func (f *flag[T]) Parse(args []string) (next []string, err error) {
	var v T
	var parsed int
	if next, v, parsed, err = f.conv(args, f); err == nil {
		f.value = &v
		f.parsed += parsed
	}
	return
}

// IsParsed retourne vrai si l’argument a déjà été parsé
func (f *flag[T]) IsParsed() bool {
	return f.parsed > 0
}

// Reset réinitialise le parsage
func (f *flag[T]) Reset() {
	f.parsed = 0
	f.value = &f.defaultval
}

// Name retourn le nom principal de l’argument nommé
func (f *flag[T]) Name() string {
	return f.name
}

// Usage retourne l’aide sur l’usage du flag
func (f *flag[T]) Usage() string {
	return f.usage
}

// ArgName retorun le nom donné à l’argument suivant le flag
func (f *flag[T]) ArgName() string {
	s := f.fval
	switch f.multiple {
	case -1:
		s += " (...)"
	case 0:
		s += " (optional)"
	case 1:
	default:
		s = fmt.Sprintf("%s (%d)", s, f.multiple)
	}
	return strings.TrimSpace(s)
}

// Flags retourne tous les flags disponibles identifiant le même flag
func (f *flag[T]) Flags() []string {
	return f.flags
}

// Value return la valeur du flag
func (f *flag[T]) Value() T {
	var out T
	if f.value != nil {
		out = *f.value
	}
	return out
}

func (f *flag[T]) capacity() int {
	return f.multiple
}

func (f *flag[T]) nbParsed() int {
	return f.parsed
}

func (f *flag[T]) def() any {
	return f.defaultval
}

func optional(fac bool) int {
	if fac {
		return 0
	}
	return 1
}

func nbArgs(n int) int {
	if n < 1 {
		return -1
	}
	return n
}

func checkArgLen(args []string, name string) error {
	if len(args) == 0 {
		return fmt.Errorf("argument needed for flag %s", name)
	}
	return nil
}

func checkCapacity(l, nb int, name string) (err error) {
	if nb > 0 && l >= nb {
		err = fmt.Errorf("overflow value for flag %s", name)
	}
	return
}

func convertArg[T any](args []string, name string, v *T) (next []string, parsed int, err error) {
	if converter.Convert(args[0], v) {
		next, parsed = args[1:], 1
	} else {
		err = fmt.Errorf("%s: invalid value for flag %s", args[0], name)
	}
	return
}

func genericArg[T any](args []string, name string) (next []string, val T, parsed int, err error) {
	if err = checkArgLen(args, name); err == nil {
		next, parsed, err = convertArg(args, name, &val)
	}
	return
}

func stringArg(args []string, name string) (next []string, val string, parsed int, err error) {
	if err = checkArgLen(args, name); err == nil {
		next, val, parsed = args[1:], args[0], 1
	}
	return
}

func boolArg(args []string, name string) (next []string, val bool, parsed int, err error) {
	if err = checkArgLen(args, name); err == nil {
		next, val, parsed = args, true, 1
	}
	return
}

func singleParse[T any](cb callbackFunc[T]) ConversionFunc[T] {
	return func(args []string, f *flag[T]) (next []string, val T, parsed int, err error) {
		return cb(args, f.name)
	}
}

func multipleParse[T any](cb callbackFunc[T]) ConversionFunc[[]T] {
	return func(args []string, f *flag[[]T]) (next []string, val []T, parsed int, err error) {
		l, n := len(*f.value), f.multiple
		if err = checkCapacity(l, n, f.name); err != nil {
			return
		}
		result := make([]T, l+1)
		if next, result[l], parsed, err = cb(args, f.name); err != nil {
			return
		}
		copy(result[:l], *f.value)
		if n > 0 {
			n--
		}
		for len(next) > 0 && n != 0 {
			a, v, p, e := cb(next, f.name)
			if e != nil {
				break
			}
			result, next = append(result, v), a
			parsed += p
			if n > 0 {
				n--
			}
		}
		val = result
		return
	}
}

// New retourne un nouvel argument nommé avec :
// - son nom,
// - son usage,
// - le nom donné à l’argument qui suit
// - un pointeur vers sa valeur
// - sa valeur par défaut si non parsé
// - son nombre d’arguments (indéfini si -1, optionnel si 0)
// - sa fonction de conversion
// - des noms supplémentaires
func New[T any](
	name, usage, argname string,
	value *T,
	defaultval T,
	nb int,
	conv ConversionFunc[T],
	others ...string,
) Flag {
	t := converter.TypeOf(value).Elem()
	if nb < 0 || nb > 1 && !t.Is(converter.Slice) {
		panic("value must be a pointer of slice")
	}
	f := &flag[T]{
		name:       name,
		flags:      append([]string{name}, others...),
		fval:       argname,
		usage:      usage,
		multiple:   nb,
		value:      value,
		defaultval: defaultval,
		conv:       conv,
	}
	f.Reset()
	return f
}

// Int retourne un argument nommé de type entier
func Int(name, usage string, value *int, defaultval int, fac bool, others ...string) Flag {
	argname, nb := "<int>", optional(fac)
	conv := singleParse(genericArg[int])
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

// Uint retourne un argument nommé de type entier non signé
func Uint(name, usage string, value *uint, defaultval uint, fac bool, others ...string) Flag {
	argname, nb := "<uint>", optional(fac)
	conv := singleParse(genericArg[uint])
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

// Float retourne un argument nommé de type décimal
func Float(name, usage string, value *float64, defaultval float64, fac bool, others ...string) Flag {
	argname, nb := "<float>", optional(fac)
	conv := singleParse(genericArg[float64])
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

// String retourne un argument nommé de type chaîne de caractères
func String(name, usage string, value *string, defaultval string, fac bool, others ...string) Flag {
	argname, nb := "<string>", optional(fac)
	conv := singleParse(stringArg)
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

// Bool retourne un argument nommé sans argument supplémentaire
func Bool(name, usage string, value *bool, defaultval bool, fac bool, others ...string) Flag {
	argname, nb := "", optional(fac)
	conv := singleParse(boolArg)
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

// IntSlice retourne un argument nommé de type tableau d’entiers
func IntSlice(name, usage string, nb int, value *[]int, defaultval []int, others ...string) Flag {
	argname, nb := "<int>", nbArgs(nb)
	conv := multipleParse(genericArg[int])
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

// UintSlice retourne un argument nommé de type tableau d’entiers non signés
func UintSlice(name, usage string, nb int, value *[]uint, defaultval []uint, others ...string) Flag {
	argname, nb := "<uint>", nbArgs(nb)
	conv := multipleParse(genericArg[uint])
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

// FloatSlice retourne un argument nommé de type tableau de décimaux
func FloatSlice(name, usage string, nb int, value *[]float64, defaultval []float64, others ...string) Flag {
	argname, nb := "<float>", nbArgs(nb)
	conv := multipleParse(genericArg[float64])
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

// StringSlice retourne un argument nommé de type tableau de strings
func StringSlice(name, usage string, nb int, value *[]string, defaultval []string, others ...string) Flag {
	argname, nb := "<string>", nbArgs(nb)
	conv := multipleParse(stringArg)
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

// BoolSlice retourne un argument nommé de type tableau de booléens
func BoolSlice(name, usage string, nb int, value *[]bool, defaultval []bool, others ...string) Flag {
	argname, nb := "", nbArgs(nb)
	conv := func(args []string, f *flag[[]bool]) (next []string, val []bool, parsed int, err error) {
		l := len(*f.value)
		val = make([]bool, l+1)
		copy(val[:l], *f.value)
		next, val[l], parsed = args, true, 1
		return
	}
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}
