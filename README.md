# shell

shell est une librairie Go pour gérer des lignes de commande Linux.

Elle est décomposée en différents sous-modules :

- console : fournit des interfaces pour gérer un prompt et implémente des invites de commandes sur l’entrée standard
- command : fournit des interfaces pour créer des commandes liées à un interpréteur de commandes
- command/flag : fournit des implémentations des interfaces définies dans command pour parser des arguments
- file : fournit des méthodes pour gérer et rechercher des informations de fichiers/répertoires
- output : fournit des fonctions pour manipuler facilement la sortie console
- scanner : permet de créer un scanner pour découper une entrée en arguments de ligne de commande
