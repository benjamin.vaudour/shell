module framagit.org/benjamin.vaudour/shell

go 1.14

require (
	framagit.org/benjamin.vaudour/collection v1.0.0
	framagit.org/benjamin.vaudour/converter v1.0.1
	framagit.org/benjamin.vaudour/strutil v1.1.0
)
