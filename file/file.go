package file

import (
	"os"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"
)

var (
	owners = make(map[string]string)
	groups = make(map[string]string)
)

type File struct {
	info     os.FileInfo
	dirname  string
	name     string
	parent   *File
	children FileList
}

func New(dirname, name string) (f *File, err error) {
	var info os.FileInfo
	info, err = os.Lstat(filepath.Join(dirname, name))
	if err == nil {
		f = &File{
			info:    info,
			dirname: dirname,
			name:    name,
		}
	}
	return
}

func NewFromInfo(info os.FileInfo, dirname string) *File {
	return &File{
		info:    info,
		dirname: dirname,
		name:    info.Name(),
	}
}

func (f *File) stat() *syscall.Stat_t { return f.info.Sys().(*syscall.Stat_t) }

func (f *File) Mode() os.FileMode { return f.info.Mode() }

func (f *File) HasMode(mask os.FileMode) bool { return f.Mode()&mask != 0 }

func (f *File) IsDir() bool { return f.info.IsDir() }

func (f *File) IsSymlink() bool { return f.HasMode(os.ModeSymlink) }

func (f *File) IsDevice() bool { return f.HasMode(os.ModeDevice) }

func (f *File) IsCharDevice() bool { return f.HasMode(os.ModeCharDevice) }

func (f *File) IsSocket() bool { return f.HasMode(os.ModeSocket) }

func (f *File) IsPipe() bool { return f.HasMode(os.ModeNamedPipe) }

func (f *File) IsIrregular() bool { return f.HasMode(os.ModeIrregular) }

func (f *File) IsRegular() bool { return f.Mode().IsRegular() }

func (f *File) IsUid() bool { return f.HasMode(os.ModeSetuid) }

func (f *File) IsGid() bool { return f.HasMode(os.ModeSetgid) }

func (f *File) IsSticky() bool { return f.HasMode(os.ModeSticky) }

func (f *File) Name() string { return f.name }

func (f *File) DirName() string { return f.dirname }

func (f *File) Path() string { return filepath.Join(f.dirname, f.name) }

func (f *File) AbsolutePath() string {
	path := f.Path()
	if filepath.IsAbs(path) {
		return path
	}
	abs, _ := filepath.Abs(path)
	return abs
}

func (f *File) Split() (dirname, name string) {
	path := filepath.Clean(f.Path())
	return filepath.Split(path)
}

func (f *File) AbsoluteSplit() (dirname, name string) {
	path := f.AbsolutePath()
	return filepath.Split(path)
}

func (f *File) Extension() string {
	if f.IsDir() {
		return ""
	}
	return filepath.Ext(f.info.Name())
}

func (f *File) LinkPath() string {
	if !f.IsSymlink() {
		return ""
	}
	lpath, _ := os.Readlink(f.Path())
	return lpath
}

func ts2Date(ts syscall.Timespec) time.Time { return time.Unix(ts.Sec, ts.Nsec) }

func (f *File) CreationTime() time.Time { return ts2Date(f.stat().Ctim) }

func (f *File) AccessTime() time.Time { return ts2Date(f.stat().Atim) }

func (f *File) ModificationTime() time.Time { return f.info.ModTime() }

func (f *File) Size() int64 { return f.info.Size() }

func (f *File) BlockSize() int64 { return f.stat().Blksize }

func (f *File) DirSize() (size int64) {
	if !f.IsDir() {
		return f.Size()
	}
	for _, c := range f.children {
		size += c.DirSize()
	}
	return
}

func (f *File) OwnerID() string { return strconv.Itoa(int(f.stat().Uid)) }

func (f *File) GroupID() string { return strconv.Itoa(int(f.stat().Gid)) }

func (f *File) Owner() string {
	id := f.OwnerID()
	if owner, ok := owners[id]; ok {
		return owner
	}
	var owner string
	if e, err := user.LookupId(id); err == nil {
		owner = e.Name
	}
	owners[id] = owner
	return owner
}

func (f *File) Group() string {
	id := f.GroupID()
	if group, ok := groups[id]; ok {
		return group
	}
	var group string
	if e, err := user.LookupGroupId(id); err == nil {
		group = e.Name
	}
	groups[id] = group
	return group
}

func (f *File) Children() FileList { return f.children }

func (f *File) SearchChildren(deepness int, searchOptions ...bool) (children FileList) {
	if !f.IsDir() || deepness == 0 {
		return
	}
	var hidden, backup bool
	if len(searchOptions) > 0 {
		hidden = searchOptions[0]
		if len(searchOptions) > 1 {
			backup = searchOptions[1]
		}
	}
	dir, err := os.Open(f.Path())
	if err != nil {
		return
	}
	defer dir.Close()
	infos, _ := dir.Readdir(0)
	dirname := f.Path()
	if deepness > 0 {
		deepness--
	}
	var wg sync.WaitGroup
	for _, i := range infos {
		name := i.Name()
		if (!hidden && strings.HasPrefix(name, ".")) || (!backup && strings.HasSuffix(name, "~")) {
			continue
		}
		child := NewFromInfo(i, dirname)
		children.Add(child)
		wg.Add(1)
		go (func(c *File) {
			defer wg.Done()
			c.SearchChildren(deepness, searchOptions...)
		})(child)
	}
	wg.Wait()
	f.children = children
	for _, c := range children {
		c.parent = f
	}
	return
}

func (f *File) Flatten(only_dirs ...bool) (fl FileList) {
	d := len(only_dirs) > 0 && only_dirs[0]
	if d && !f.IsDir() {
		return
	}
	fl.Add(f)
	for _, c := range f.children {
		fl.Add(c.Flatten(only_dirs...)...)
	}
	return
}

func (f *File) Parent() *File {
	return f.parent
}

func (f *File) SearchParent() *File {
	if f.parent != nil {
		return f.parent
	}
	d, _ := f.AbsoluteSplit()
	pdirname, pname := filepath.Split(d)
	p, err := New(pdirname, pname)
	if err != nil {
		return nil
	}
	f.parent = p
	return p
}

func (f *File) AddChildren(children ...*File) {
	for _, c := range children {
		c.parent = f
	}
	f.children.Add(children...)
}

func (f *File) SetParent(p *File, both ...bool) {
	f.parent = p
	if p != nil && len(both) > 0 && both[0] {
		p.children.Add(f)
	}
}
