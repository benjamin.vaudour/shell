package output

import (
	"fmt"
	"strings"
	"syscall"
	"unicode"
	"unicode/utf8"
	"unsafe"
)

func VisibleWidth(str string) int {
	w := 0
	for len(str) > 0 {
		if strings.HasPrefix(str, "\033[") {
			i := strings.Index(str, "m")
			if i < 0 {
				break
			}
			str = str[i+1:]
			continue
		}
		r, i := utf8.DecodeRuneInString(str)
		if unicode.IsPrint(r) {
			w++
		}
		str = str[i:]
	}
	return w
}

func ClearBeginOfLine() { fmt.Print("\033[1K") } // Curseur compris
func ClearEndOfLine()   { fmt.Print("\033[0K") } // Curseur compris
func ClearLine()        { fmt.Print("\033[2K") }

func ClearBeginOfScreen()       { fmt.Print("\033[1J") } // Curseur compris
func ClearEndOfScreen()         { fmt.Print("\033[0J") } // Curseur compris
func ClearAllScreen()           { fmt.Print("\033[2J") }
func ClearAllScreenAndHistory() { fmt.Print("\033[3J") }

func MoveUp(c int)    { fmt.Printf("\033[%dA", c) }
func MoveDown(c int)  { fmt.Printf("\033[%dB", c) }
func MoveLeft(c int)  { fmt.Printf("\033[%dD", c) }
func MoveRight(c int) { fmt.Printf("\033[%dC", c) }

func MoveLineUp(c int)   { fmt.Printf("\033[%dF", c) }
func MoveLineDown(c int) { fmt.Printf("\033[%dE", c) }

func MoveToColumn(c int)    { fmt.Printf("\033[%dG", c) }      // Commence à 1
func MoveToScreen(l, c int) { fmt.Print("\033[%d;%dH", l, c) } // Commence à 1
func MoveTopLeft()          { MoveToScreen(1, 1) }

func SaveCursorPosition()    { fmt.Print("\033[s") }
func RestoreCursorPosition() { fmt.Print("\033[u") }

func NewLines(c int) {
	for c > 0 {
		fmt.Print("\n")
		c--
	}
}

type TerminalSize struct {
	row, col       uint16
	xpixel, ypixel uint16
}

func (ts TerminalSize) Width() int  { return int(ts.col) }
func (ts TerminalSize) Height() int { return int(ts.row) }

func GetTerminalSize() (ts TerminalSize, ok bool) {
	sc, _, _ := syscall.Syscall(syscall.SYS_IOCTL, uintptr(syscall.Stdout), syscall.TIOCGWINSZ, uintptr(unsafe.Pointer(&ts)))
	ok = int(sc) >= 0
	return
}
