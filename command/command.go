package command

import (
	"strings"

	"framagit.org/benjamin.vaudour/strutil/align"
	"framagit.org/benjamin.vaudour/strutil/style"
)

//Parser est une interface pour parser des arguments en ligne de commande
type Parser interface {
	Parse([]string) ([]string, error)
	IsParsed() bool
	Reset()
}

//Helper est une interface permettant d’afficher l’aide d’une ligne de commande
type Helper interface {
	Name() string
	Usage() string
}

//Command représente une commande à saisir
type Command interface {
	Parser
	Helper
	Help() string
	Flags() FlagSet
	Run()
}

//CommandSet est une commande regroupant plusieurs sous-commandes
type CommandSet interface {
	Parser
	Helper
	Help() string
	Commands() []Command
}

func CommandHelp(c Command) string {
	h := c.Help()
	if h != "" {
		return h
	}
	var sb strings.Builder
	sb.WriteString(c.Usage())
	sb.WriteByte('\n')
	sb.WriteString(style.Format("Usage:", "l_yellow", "underline"))
	sb.WriteByte(' ')
	sb.WriteString(style.New("l_yellow").String())
	sb.WriteString(c.Name())
	sb.WriteString(" <options> <args>\n")
	sb.WriteString(style.New().String())
	if fs := c.Flags(); fs != nil {
		sb.WriteByte('\n')
		sb.WriteString(fs.Help())
	}
	return sb.String()
}

func CommandSetHelp(cs CommandSet) string {
	h := cs.Help()
	if h != "" {
		return h
	}
	var sb strings.Builder
	sb.WriteString(cs.Usage())
	sb.WriteByte('\n')
	sb.WriteString(style.Format("Usage:", "l_yellow", "underline"))
	sb.WriteByte(' ')
	sb.WriteString(style.New("l_yellow").String())
	sb.WriteString(cs.Name())
	sb.WriteString(" <cmd> <args>\n\n")
	sb.WriteString(style.New().String())
	sb.WriteString(style.Format("Available commands:\n", "l_red"))
	cmds := cs.Commands()
	left, right := make([]string, len(cmds)), make([][]string, len(cmds))
	n := 0
	has_help := false
	for i, c := range cmds {
		has_help = has_help || c.Name() == "help"
		left[i] = align.TabulateLeft(c.Name(), 2)
		if l := len(left[i]); l+1 > n {
			n = l + 1
		}
		right[i] = strings.Split(c.Usage(), "\n")
	}
	if !has_help {
		left = append(left, align.TabulateLeft("help", 2))
		if n < 7 {
			n = 7
		}
		right = append(right, []string{"Print this help"})
	}
	for i := range cmds {
		sb.WriteString(left[i])
		for j, u := range right[i] {
			if j != 0 {
				sb.WriteString(align.Tab(n))
			}
			sb.WriteString(u)
			sb.WriteByte('\n')
		}
	}
	return sb.String()
}
