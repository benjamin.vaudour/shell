package flag

import (
	"fmt"
	"strings"

	"framagit.org/benjamin.vaudour/collection"
	"framagit.org/benjamin.vaudour/shell/command"
	"framagit.org/benjamin.vaudour/strutil/align"
	"framagit.org/benjamin.vaudour/strutil/style"
)

//FlagSet et un ensemble de flags
type FlagSet struct {
	flags       []*Flag
	fmap        map[string]*Flag
	args        []*Arg
	unix        bool
	parsed      bool
	help_needed bool
}

func (fs *FlagSet) Parse(args []string) (next []string, err error) {
	var nflag []string
	if fs.unix {
		args = formatArgs(args)
		set, _ := collection.SliceToSet(args)
		flags := collection.NewSet()
		for f := range fs.fmap {
			flags.Add(f)
		}
		if (set.Contains("-h") && !flags.Contains("-h")) || (set.Contains("--help") && !set.Contains("--help")) {
			fs.parsed = true
			fs.help_needed = true
			return
		}
		for i, e := range args {
			if e == "--" {
				args, nflag = args[:i], args[i+1:]
				break
			}
		}
	}
	for len(args) > 0 {
		e := args[0]
		f, ok := fs.fmap[e]
		if !ok {
			if fs.unix && (isShort(e) || isLong(e)) {
				err = fmt.Errorf("Invalid flag: %s", e)
				return
			}
			break
		}
		args = args[1:]
		ni := len(args)
		for i, e := range args {
			if _, ok := fs.fmap[e]; ok {
				ni = i
				break
			}
		}
		na := args[:ni]
		if na, err = f.Parse(na); err != nil {
			return
		}
		args = append(na, args[ni:]...)
	}
	if len(args) > 0 {
		nflag = append(args, nflag...)
	}
	i := 0
	for len(nflag) > 0 && i < len(fs.args) {
		if nflag, err = fs.args[i].Parse(nflag); err != nil {
			return
		}
		i++
	}
	if len(nflag) > 0 {
		err = fmt.Errorf("Too much args given")
		return
	}
	for _, a := range fs.args {
		if a.nb > 0 && len(*a.value) < a.nb {
			err = fmt.Errorf("Not enough args for %s", a.name)
			return
		}
	}
	for _, f := range fs.flags {
		if f.multiple > 0 && f.parsed != f.multiple {
			err = fmt.Errorf("Mismatch number of args for flag %s: required (%d), has (%d)", f.name, f.multiple, f.parsed)
			return
		}
	}
	fs.parsed = true
	return
}

func (fs *FlagSet) IsParsed() bool { return fs.parsed }

func (fs *FlagSet) Reset() {
	fs.help_needed = false
	fs.parsed = false
	for _, f := range fs.flags {
		f.Reset()
	}
	for _, a := range fs.args {
		a.Reset()
	}
}

//HelpRequired retourne vrai si le parsage contient un flag de type aide
func (fs *FlagSet) HelpRequired() bool { return fs.help_needed }

func (fs *FlagSet) Flags() []command.Flag {
	out := make([]command.Flag, len(fs.flags))
	for i, f := range fs.flags {
		out[i] = f
	}
	return out
}

func (fs *FlagSet) Args() []string {
	var out []string
	for _, a := range fs.args {
		out = append(out, *a.value...)
	}
	return out
}

func (fs *FlagSet) _helpu() string {
	short, long, usage := make([]string, len(fs.flags)), make([]string, len(fs.flags)), make([]string, len(fs.flags))
	hs, hl := true, true
	var n int
	for i, f := range fs.flags {
		for _, e := range f.flags {
			if isShort(e) {
				short[i] = e
				hs = hs && e != "-h"
			} else {
				long[i] = " " + e
				hl = hl && e != "--help"
				if l := len(e); l >= n {
					n = l
				}
			}
		}
		usage[i] = f.usage
	}
	if hs || hl {
		usage = append(usage, "Print this help")
		var s, l string
		if hs {
			s = "-h"
		}
		if hl {
			l = " --help"
			if n < 6 {
				n = 6
			}
		}
		short = append(short, s)
		long = append(long, l)
	}
	for i, s := range short {
		if s != "" && long[i] != "" {
			short[i] = s + ","
		} else {
			short[i] = s + " "
		}
	}
	var sb strings.Builder
	sb.Grow(100 * len(fs.flags))
	for i, f := range fs.flags {
		sb.WriteString(align.Right(short[i], 5))
		sb.WriteString(align.Left(long[i], n+3))
		sb.WriteString(style.Format(fmt.Sprintf("Arg: %s [default: %v]\n", f.ArgName(), f.defaultval), "l_yellow"))
		for _, u := range strings.Split(usage[i], "\n") {
			sb.WriteString(align.Tab(n + 10))
			sb.WriteString(u)
			sb.WriteByte('\n')
		}
	}
	return sb.String()
}

func (fs *FlagSet) _help() string {
	var n int
	left, right := make([]string, len(fs.flags)), make([][]string, len(fs.flags))
	for i, f := range fs.flags {
		left[i] = align.TabulateLeft(fmt.Sprintf("%s %s  ", f.name, f.ArgName()), 2)
		if l := len(left[i]); n < l {
			n = l
		}
		right[i] = append(right[i], style.Format(fmt.Sprint("default: %v\n", f.defaultval), "l_yellow"))
		right[i] = append(right[i], strings.Split(f.usage, "\n")...)
		if l := len(f.flags); l > 1 {
			right[i] = append(right[i], style.Format(fmt.Sprintf("others flags: %s", strings.Join(f.flags[:l-1], ", ")), "italic"))
		}
	}
	var sb strings.Builder
	sb.Grow(100 * len(fs.flags))
	for i := range fs.flags {
		sb.WriteString(align.Left(left[i], n))
		sb.WriteString(right[i][0])
		for _, u := range right[i][1:] {
			sb.WriteString(align.Tab(n))
			sb.WriteString(u)
			sb.WriteByte('\n')
		}
	}
	return sb.String()
}

func (fs *FlagSet) _args() string {
	left, right := make([]string, len(fs.args)), make([][]string, len(fs.args))
	n := 0
	for i, a := range fs.args {
		left[i] = align.TabulateLeft(a.name+"  ", 2)
		if l := len(left[i]); l > n {
			n = l
		}
		right[i] = strings.Split(a.usage, "\n")
	}
	var sb strings.Builder
	sb.Grow(100 * len(fs.args))
	for i := range fs.args {
		sb.WriteString(left[i])
		for i, u := range right[i] {
			if i != 0 {
				sb.WriteString(align.Tab(n))
			}
			sb.WriteString(u)
			sb.WriteByte('\n')
		}
	}
	return sb.String()
}

func (fs *FlagSet) Help() string {
	var sb strings.Builder
	if len(fs.flags) > 0 {
		sb.WriteString(style.Format("Options:\n", "l_red"))
		if fs.unix {
			sb.WriteString(fs._helpu())
		} else {
			sb.WriteString(fs._help())
		}
	}
	if len(fs.args) > 0 {
		if len(fs.flags) > 0 {
			sb.WriteByte('\n')
		}
		sb.WriteString(style.Format("Arguments:\n", "l_red"))
		sb.WriteString(fs._args())
	}
	return sb.String()
}

//NewSet retourne un nouveau set de flags
//Si unix, les arguments nommés doivent être de type
//unix : -n pour les noms courts et --name pour les noms longs
func NewSet(unix bool) *FlagSet {
	return &FlagSet{
		unix: unix,
		fmap: make(map[string]*Flag),
	}
}

func (fs *FlagSet) Add(flags ...*Flag) *FlagSet {
	for _, f := range flags {
		for _, e := range f.Flags() {
			if _, exists := fs.fmap[e]; exists {
				panic(fmt.Sprintf("Flag %s is already defined", e))
			}
			fs.fmap[e] = f
		}
		fs.flags = append(fs.flags, f)
	}
	return fs
}

func (fs *FlagSet) AddArgs(args ...*Arg) *FlagSet {
	var la *Arg
	if len(fs.args) > 0 {
		la = fs.args[0]
	}
	for _, a := range args {
		if la != nil && la.nb < 1 {
			panic("facultative argument must be unique and last located")
		}
		la = a
	}
	fs.args = append(fs.args, args...)
	return fs
}
