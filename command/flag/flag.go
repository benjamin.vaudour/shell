package flag

import (
	"fmt"
	"strings"

	"framagit.org/benjamin.vaudour/collection"
	convert "framagit.org/benjamin.vaudour/converter"
)

//ConversionFunc est une fonction de conversion
//Les éléments retournés sont :
//- next : liste des éléments restants à parser
//- val : la valeur parsée
//- err : une erreur si le parsage échoue
type ConversionFunc func(args []string) (next []string, val interface{}, parsed int, err error)

//Flag représente un argument nommé de commande
type Flag struct {
	name       string
	flags      []string
	fval       string
	usage      string
	multiple   int
	value      interface{}
	defaultval interface{}
	parsed     int
	conv       ConversionFunc
}

//Parse parse l’argument à partir de l’entrée fournie
//et retourne les éléments restant à parser.
//Si le parsage échoue, une erreur est retournée.
func (f *Flag) Parse(args []string) (next []string, err error) {
	var v interface{}
	var parsed int
	if next, v, parsed, err = f.conv(args); err == nil {
		if ok := convert.Convert(v, f.value, true); !ok {
			err = fmt.Errorf("%v cannot be a value of flag %s", v, f.Name())
		} else {
			f.parsed += parsed
		}
	}
	return
}

//IsParsed retourne vrai si l’argument a déjà été parsé
func (f *Flag) IsParsed() bool { return f.parsed > 0 }

//Reset réinitialise le parsage
func (f *Flag) Reset() {
	f.parsed = 0
	if !convert.Convert(f.defaultval, f.value) {
		convert.SetZero(f.value)
	}
}

func (f *Flag) Name() string { return f.name }

func (f *Flag) Usage() string { return f.usage }

func (f *Flag) ArgName() string {
	s := f.fval
	switch f.multiple {
	case -1:
		s += " (...)"
	case 0:
		s += " (optional)"
	case 1:
	default:
		s = fmt.Sprintf("%s (%d)", s, f.multiple)
	}
	return strings.TrimSpace(s)
}

//Flags retourne les noms possibles permettant d’identifier l’argument
func (f *Flag) Flags() []string { return f.flags }

//Value retourne la valeur parsée de l’argument
func (f *Flag) Value() interface{} { return convert.ValueOf(f.value) }

//New retourne un nouvel argument nommé avec :
//- son nom,
//- son usage,
//- le nom donné à l’argument qui suit
//- un pointeur vers sa valeur
//- sa valeur par défaut si non parsé
//- son nombre d’arguments (indéfini si -1, optionnel si 0)
//- sa fonction de conversion
//- des noms supplémentaires
func New(name, usage, argname string, value, defaultval interface{}, nb int, conv ConversionFunc, others ...string) *Flag {
	if !convert.IsPointer(value) {
		panic("value must be a pointer")
	}
	if nb < 0 && nb > 1 && !convert.IsSlice(convert.ValueOf(value)) {
		panic("value must be a pointer of slice")
	}
	f := &Flag{
		name:       name,
		flags:      append([]string{name}, others...),
		fval:       argname,
		usage:      usage,
		multiple:   nb,
		value:      value,
		defaultval: defaultval,
		conv:       conv,
	}
	f.Reset()
	return f
}

func _checkArgLen(args []string, name string) error {
	if len(args) == 0 {
		return fmt.Errorf("argument needed for flag %s", name)
	}
	return nil
}

func _checkConv(args []string, name string, v interface{}) (next []string, val interface{}, parsed int, err error) {
	if convert.Convert(args[0], v) {
		next, val, parsed = args[1:], convert.ValueOf(v), 1
	} else {
		err = fmt.Errorf("%s: invalid value for flag %s", args[0], name)
	}
	return
}

func _i(args []string, name string) (next []string, val interface{}, parsed int, err error) {
	if err = _checkArgLen(args, name); err == nil {
		var v int
		return _checkConv(args, name, &v)
	}
	return
}
func _u(args []string, name string) (next []string, val interface{}, parsed int, err error) {
	if err = _checkArgLen(args, name); err == nil {
		var v uint
		return _checkConv(args, name, &v)
	}
	return
}
func _f(args []string, name string) (next []string, val interface{}, parsed int, err error) {
	if err = _checkArgLen(args, name); err == nil {
		var v float64
		return _checkConv(args, name, &v)
	}
	return
}
func _s(args []string, name string) (next []string, val interface{}, parsed int, err error) {
	if err = _checkArgLen(args, name); err == nil {
		next, val, parsed = args[1:], args[0], 1
	}
	return
}

func _checkLen(l, nb int, name string) (err error) {
	if nb > 0 && l >= nb {
		err = fmt.Errorf("overflow value for flag %s", name)
	}
	return
}

func _a(args []string, name string, l, n int, c func([]string, string) ([]string, interface{}, int, error)) (next []string, val interface{}, parsed int, err error) {
	if err = _checkLen(l, n, name); err != nil {
		return
	}
	var e interface{}
	if next, e, parsed, err = c(args, name); err != nil {
		return
	}
	out := []interface{}{e}
	if n > 0 {
		n--
	}
	next = args[1:]
	var i = 0
	for i < len(next) && n != 0 {
		var err2 error
		if _, e, parsed, err2 = c(next[i:], name); err2 != nil {
			break
		}
		out = append(out, e)
		i++
		parsed++
		if n > 0 {
			n--
		}
	}
	next, val = next[i:], out
	return
}

func _fac(f bool) int {
	if f {
		return 0
	}
	return 1
}

func _n(n int) int {
	if n < 1 {
		return -1
	}
	return n
}

//Int retourne un argument nommé de type entier
func Int(name, usage string, value *int, defaultval int, fac bool, others ...string) *Flag {
	argname, nb := "<int>", _fac(fac)
	conv := func(args []string) (next []string, val interface{}, parsed int, err error) {
		return _i(args, name)
	}
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

//Uint retourne un argument nommé de type entier non signé
func Uint(name, usage string, value *uint, defaultval uint, fac bool, others ...string) *Flag {
	argname, nb := "<uint>", _fac(fac)
	conv := func(args []string) (next []string, val interface{}, parsed int, err error) {
		return _u(args, name)
	}
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

//Flot retourne un argument nommé de type décimal
func Float(name, usage string, value *float64, defaultval float64, fac bool, others ...string) *Flag {
	argname, nb := "<float>", _fac(fac)
	conv := func(args []string) (next []string, val interface{}, parsed int, err error) {
		return _f(args, name)
	}
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

//String retourne un argument nommé de type chaîne de caractères
func String(name, usage string, value *string, defaultval string, fac bool, others ...string) *Flag {
	argname, nb := "<string>", _fac(fac)
	conv := func(args []string) (next []string, val interface{}, parsed int, err error) {
		return _s(args, name)
	}
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

//Int retourne un argument nommé sans argument supplémentaire
func Bool(name, usage string, value *bool, defaultval bool, fac bool, others ...string) *Flag {
	argname, nb := "", _fac(fac)
	conv := func(args []string) (next []string, val interface{}, parsed int, err error) {
		next, val, parsed = args, true, 1
		return
	}
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

//IntSlice retourne un argument nommé de type tableau d’entiers
func IntSlice(name, usage string, nb int, value *[]int, defaultval []int, others ...string) *Flag {
	argname, nb := "<int>", _n(nb)
	conv := func(args []string) (next []string, val interface{}, parsed int, err error) {
		if next, val, parsed, err = _a(args, name, len(*value), nb, _i); err == nil {
			collection.Add(value, collection.Slice(val)...)
			val = *value
		}
		return
	}
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

//UintSlice retourne un argument nommé de type tableau d’entiers non signés
func UintSlice(name, usage string, nb int, value *[]uint, defaultval []uint, others ...string) *Flag {
	argname, nb := "<uint>", _n(nb)
	conv := func(args []string) (next []string, val interface{}, parsed int, err error) {
		if next, val, parsed, err = _a(args, name, len(*value), nb, _u); err == nil {
			collection.Add(value, collection.Slice(val)...)
			val = *value
		}
		return
	}
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

//FloatSlice retourne un argument nommé de type tableau de nombres
func FloatSlice(name, usage string, nb int, value *[]float64, defaultval []float64, others ...string) *Flag {
	argname, nb := "<float>", _n(nb)
	conv := func(args []string) (next []string, val interface{}, parsed int, err error) {
		if next, val, parsed, err = _a(args, name, len(*value), nb, _f); err == nil {
			collection.Add(value, collection.Slice(val)...)
			val = *value
		}
		return
	}
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

//StringSlice retourne un argument nommé de type tableau de strings
func StringSlice(name, usage string, nb int, value *[]string, defaultval []string, others ...string) *Flag {
	argname, nb := "<string>", _n(nb)
	conv := func(args []string) (next []string, val interface{}, parsed int, err error) {
		if next, val, parsed, err = _a(args, name, len(*value), nb, _s); err == nil {
			collection.Add(value, collection.Slice(val)...)
			val = *value
		}
		return
	}
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}

//BoolSlice retourne un argument nommé de type tableau de booléens
func BoolSlice(name, usage string, nb int, value *[]bool, defaultval []bool, others ...string) *Flag {
	argname, nb := "", _n(nb)
	conv := func(args []string) (next []string, val interface{}, parsed int, err error) {
		next, val, parsed = args, append(*value, true), 1
		return
	}
	return New(name, usage, argname, value, defaultval, nb, conv, others...)
}
