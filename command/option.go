package command

//Flag est une interface réprsentant des arguments nommés d’une ligne de commande
type Flag interface {
	Parser
	Helper
	ArgName() string
	Flags() []string
	Value() interface{}
}

//FlagSet est un ensemble de flags
type FlagSet interface {
	Parser
	Help() string
	HelpRequired() bool
	Flags() []Flag
	Args() []string
}
