package console

import (
	"fmt"
	"io"
	"os"
)

//Error représente une sortie de commande UNIX
type Error struct {
	msg  string
	code int
}

func (e *Error) Error() string {
	return e.msg
}

func (e *Error) Code() int { return e.code }

func (e *Error) Exit(wtr ...io.Writer) {
	var w io.Writer
	w = os.Stderr
	if len(wtr) > 0 {
		w = wtr[0]
	}
	s := e.Error()
	if len(s) > 0 {
		fmt.Fprintln(w, s)
	}
	os.Exit(e.code)
}

func NewError(code int, msg string) *Error {
	return &Error{
		msg:  msg,
		code: code,
	}
}

func NewErrorf(code int, form string, args ...interface{}) *Error {
	return NewError(code, fmt.Sprintf(form, args...))
}

func Exit(code int, msg string) {
	NewError(code, msg).Exit()
}

func Exitf(code int, form string, args ...interface{}) {
	NewErrorf(code, form, args...).Exit()
}
