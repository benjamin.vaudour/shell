package console

//CompleteFunc est une fonction permettant de compléter une ligne de commandes
type CompleteFunc func(line string) []string

//WordCompleteFunc est une fonction permettant de compléter un arguement dans une ligne de commande
//line est la ligne d’arguments saisis
//pos est la position actuelle du curseur
//head et tail sont la partie à gauche et à droite de l’argument à compléter
//candidates est la liste des choix possibles pour compléter l’argument
type WordCompleteFunc func(line string, pos int) (head string, candidates []string, tail string)
