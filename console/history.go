package console

import (
	"bufio"
	"fmt"
	"io"
	"unicode/utf8"
)

//History est une interface pour gérer l’historique des commandes saisies
type History interface {
	AppendHistory(string)
	ClearHistory()
	ReadHistory(io.Reader) (int, error)
	WriteHistory(io.Writer) (int, error)
}

type _history []string

func (h *_history) AppendHistory(s string) { *h = append(*h, s) }

func (h *_history) ClearHistory() { *h = []string{} }

func (h *_history) ReadHistory(r io.Reader) (n int, err error) {
	buf := bufio.NewReader(r)
	var line []byte
	var part bool
	for {
		line, part, err = buf.ReadLine()
		if err == nil {
			if part {
				err = fmt.Errorf("line %d is too long", n+1)
			} else if !utf8.Valid(line) {
				err = fmt.Errorf("invalid string at line %d", n+1)
			} else {
				h.AppendHistory(string(line))
				n++
			}
		}
		if err != nil {
			if err == io.EOF {
				err = nil
			}
			break
		}
	}
	return
}

func (h *_history) WriteHistory(w io.Writer) (n int, err error) {
	for _, item := range *h {
		_, err := fmt.Fprintln(w, item)
		if err != nil {
			break
		}
		n++
	}
	return
}
