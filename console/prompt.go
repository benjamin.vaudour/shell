package console

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"framagit.org/benjamin.vaudour/collection"
	convert "framagit.org/benjamin.vaudour/converter"
	"framagit.org/benjamin.vaudour/shell/scanner"
)

//PromptFunc est une fonction qui affiche une invite de commande et retourne la saisie brute
type PromptFunc func(string) (string, error)

//ConvertFunc est une fonction qui affiche une invite de commande et retourne la saisie parsée
type ConvertFunc func(string) (interface{}, error)

type ValidFunc func(interface{}) bool

//Int lance une invite de commande attendant un entier
func (pf PromptFunc) Int(p string) (result int, err error) {
	var s string
	s, err = pf(p)
	if err == nil {
		result, err = strconv.Atoi(s)
	}
	return
}

//Bool lance une invite de commande attendant un booléen
func (pf PromptFunc) Bool(p string) (result bool, err error) {
	var s string
	s, err = pf(p)
	if err == nil {
		result, err = strconv.ParseBool(s)
	}
	return
}

//Slice lance une invite de commande attendant une liste d’arguments
func (pf PromptFunc) Slice(p string, t ...scanner.Tokenizer) (result []string, err error) {
	var s string
	s, err = pf(p)
	if err == nil {
		r := strings.NewReader(s)
		sc := scanner.NewScanner(r, t...)
		for sc.Scan() {
			result = append(result, sc.Text())
		}
	}
	return
}

//Slice lance une invite de commande attendant une réponse optionnelle
func (pf PromptFunc) Default(p string, value interface{}, def interface{}, conv ConvertFunc, valid ValidFunc) error {
	if !convert.IsPointer(value) {
		return fmt.Errorf("%v is not a pointer", value)
	}
	if convert.IsNil(value) {
		return fmt.Errorf("%v is nil", value)
	}
	set := func(n interface{}) error {
		if !convert.Convert(n, value, true) {
			return fmt.Errorf("%v cannot be assigned to %v", n, value)
		}
		return nil
	}
	if s, err := pf(p); err == nil {
		if n, err := conv(s); err == nil && valid(n) {
			if set(n) == nil {
				return nil
			}
		}
	}
	return set(def)
}

var rl PromptFunc = func(p string) (result string, err error) {
	fmt.Print(p)
	sc := bufio.NewScanner(os.Stdin)
	if sc.Scan() {
		result = sc.Text()
	} else {
		err = sc.Err()
	}
	return
}

func Read(prompt string) (result string, err error) { return rl(prompt) }

func ReadInt(prompt string) (result int, err error) { return rl.Int(prompt) }

func ReadBool(prompt string) (result bool, err error) { return rl.Bool(prompt) }

func ReadSlice(prompt string, t ...scanner.Tokenizer) (result []string, err error) {
	return rl.Slice(prompt, t...)
}

func Question(question string, def string) (result string) {
	p := fmt.Sprintf("%s (default: %v) ", question, def)
	if r, err := Read(p); err == nil && r != "" {
		return r
	}
	return def
}

func QuestionInt(question string, def int) (result int) {
	p := fmt.Sprintf("%s (default: %v) ", question, def)
	if r, err := ReadInt(p); err == nil {
		return r
	}
	return def
}

func QuestionChoice(question string, def string, choices []string) (result string) {
	set, _ := collection.SliceToSet(choices)
	p := fmt.Sprintf("%s [%s] (default: %v) ", question, strings.Join(choices, "|"), def)
	if r, err := Read(p); err == nil && set.Contains(r) {
		return r
	}
	return def
}

func QuestionBool(question string, def bool) (result bool) {
	choices := "o/N"
	if def {
		choices = "O/n"
	}
	p := fmt.Sprintf("%s [%s] ", question, choices)
	if r, err := Read(p); err == nil && len(r) > 0 {
		switch r[0] {
		case 'Y', 'y', 'O', 'o', '1':
			return true
		case 'N', 'n', '0':
			return false
		}
	}
	return def
}
