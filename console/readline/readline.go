package readline

// #cgo LDFLAGS: -lreadline
// #include <stdio.h>
// #include <stdlib.h>
// #include <readline/readline.h>
// #include <readline/history.h>
import "C"

import (
	"bufio"
	"fmt"
	"io"
	"unicode/utf8"
	"unsafe"
)

type Line struct {
	history []string
}

func (l *Line) AppendHistory(s string) {
	l.history = append(l.history, s)
	cs := C.CString(s)
	C.add_history(cs)
	C.free(unsafe.Pointer(cs))
}

func (l *Line) ClearHistory() {
	l.history = []string{}
	C.clear_history()
}

func (l *Line) ReadHistory(r io.Reader) (n int, err error) {
	buf := bufio.NewReader(r)
	var line []byte
	var part bool
	for {
		line, part, err = buf.ReadLine()
		if err == nil {
			if part {
				err = fmt.Errorf("line %d is too long", n+1)
			} else if !utf8.Valid(line) {
				err = fmt.Errorf("invalid string at line %d", n+1)
			} else {
				l.AppendHistory(string(line))
				n++
			}
		}
		if err != nil {
			if err == io.EOF {
				err = nil
			}
			break
		}
	}
	return
}

func (l *Line) WriteHistory(w io.Writer) (n int, err error) {
	for _, item := range l.history {
		_, err := fmt.Fprintln(w, item)
		if err != nil {
			break
		}
		n++
	}
	return
}

func (l *Line) Prompt(prompt string) (result string, err error) {
	cprompt := C.CString(prompt)
	cresult := C.readline(cprompt)
	C.free(unsafe.Pointer(cprompt))
	if cresult == nil {
		err = fmt.Errorf("No result given") // EOF
	} else {
		result = C.GoString(cresult)
		C.free(unsafe.Pointer(cresult))
	}
	return
}
