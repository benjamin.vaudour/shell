package console

//Prompter est une interface pour représenter un prompt
type Prompter interface {
	Prompt(string) (string, error)
}

//Terminal est un prompt stockant l’historique des saisies
type Terminal interface {
	Prompter
	History
}

//CompleteTerminal est un prompt implémentant l’autocomplétion de lignes
type CompleteTerminal interface {
	Terminal
	SetCompleter(CompleteFunc)
}

//WordCompleteTerminal est un prompt implémentant l’autocomplétion de mots
type WordCompleteTerminal interface {
	Terminal
	SetWordCompleter(WordCompleteFunc)
}

//AutoCompleteTerminal est un prompt implémentant l’autocomplétion de lignes et de mots
type AutoCompleteTerminal interface {
	Terminal
	SetCompleter(CompleteFunc)
	SetWordCompleter(WordCompleteFunc)
}

func PromptOf(p Prompter) PromptFunc { return p.Prompt }

type _sterm struct {
	p PromptFunc
	e bool
	History
}

func (t *_sterm) Prompt(p string) (result string, err error) {
	result, err = t.p(p)
	if t.e || err == nil {
		t.AppendHistory(result)
	}
	return
}

//New retourne un terminal élémentaire
//Si historyOnError, seules les commandes sans erreur sont stockées dans l’historique
func New(historyOnError ...bool) Terminal {
	t := _sterm{
		p:       Read,
		History: new(_history),
	}
	if len(historyOnError) > 0 {
		t.e = historyOnError[0]
	}
	return &t
}
